package com.hypermine.habbo.gui;

import com.hypermine.habbo.gui.controllers.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Scott Stamp <scott@hypermine.com> on 3/20/2017.
 */
public class Main extends Application {
    public Thread executorThread;

    @Override
    public void start(Stage primaryStage) throws Exception {
        MainController mainController = new MainController(executorThread, primaryStage);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("views/main.fxml"));
        loader.setController(mainController);
        Scene s = new Scene(loader.load(), -1, -1);
        s.getStylesheets().add("main/resources/ThemeDark.css");

        primaryStage.setTitle("SpriteExtractor [Main] - Hypermine Solutions");
        primaryStage.setScene(s);
        primaryStage.setResizable(false);
        primaryStage.show();

        mainController.setSavePath();
    }

    @Override
    public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
