package com.hypermine.habbo.gui.controllers;

import com.hypermine.habbo.SpriteExtractor;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.commons.validator.routines.UrlValidator;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Scott Stamp <scott@hypermine.com> on 3/20/2017.
 */
public class MainController implements Initializable {
    private Stage primaryStage;
    private StatusController statusController = new StatusController();
    private UrlValidator urlValidator = new UrlValidator(new String[] {"http","https"}, UrlValidator.ALLOW_ALL_SCHEMES);
    private Thread executorThread;

    @FXML GridPane gpOptions;
    @FXML ComboBox<String> cmbHotel;
    @FXML TextField txtCustomUrl;
    @FXML CheckBox chkDownloadMisc;
    @FXML CheckBox chkDownloadPets;
    @FXML CheckBox chkDownloadFigures;
    @FXML CheckBox chkDownloadEffects;
    @FXML CheckBox chkDownloadMasks;
    @FXML CheckBox chkDownloadFurni;
    @FXML CheckBox chkExtractSprites;
    @FXML CheckBox chkExtractIcons;
    @FXML TextField txtSavePath;
    @FXML Button btnStartTasks;
    private boolean validCustomUrl = true;

    public MainController(Thread executorThread, Stage primaryStage) {
        this.executorThread = executorThread;
        this.primaryStage = primaryStage;
    }

    public void setSavePath() {
        //txtSavePath.setText(System.getProperty("user.dir"));
        txtSavePath.setText("R:\\visdata");
    }

    public void startTasks() throws Exception {

        if (!validCustomUrl)
            return;

        FXMLLoader loader = new FXMLLoader();
        loader.setController(statusController);
        Parent root = loader.load(
                getClass().getClassLoader().getResourceAsStream("com/hypermine/habbo/gui/views/status.fxml"));

        Scene scene = new Scene(root, -1, -1);
        scene.getStylesheets().add("main/resources/ThemeDark.css");

        primaryStage.setTitle("SpriteExtractor [Log] - Hypermine Solutions");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

        statusController.log(cmbHotel.getValue());

        String hotel = txtCustomUrl.getText();
        if (hotel.equals(""))
        {
            switch (cmbHotel.getValue())
            {
                case "Habbo.com":
                    hotel = "https://www.habbo.com/gamedata/external_variables/1";
                    break;
                case "Habbo.nl":
                    hotel = "https://www.habbo.nl/gamedata/external_variables/1";
                    break;
            }
        }

        SpriteExtractor extractor = new SpriteExtractor(txtSavePath.getText(),
                hotel,
                statusController,
                chkDownloadMisc.isSelected(),
                chkDownloadPets.isSelected(),
                chkDownloadFigures.isSelected(),
                chkDownloadEffects.isSelected(),
                chkDownloadMasks.isSelected(),
                chkDownloadFurni.isSelected(),
                chkExtractSprites.isSelected(),
                chkExtractIcons.isSelected());

        executorThread = new Thread(extractor);
        executorThread.start();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cmbHotel.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals("Custom")) txtCustomUrl.setDisable(false);
            else
            {
                validCustomUrl = true;
                txtCustomUrl.setDisable(true);
            }
        });

        txtCustomUrl.textProperty().addListener((observable, oldValue, newValue) -> {
            validCustomUrl = urlValidator.isValid(newValue);
            if (validCustomUrl)
                txtCustomUrl.getStyleClass().removeAll("error");
            else txtCustomUrl.getStyleClass().add("error");
        });
    }
}
