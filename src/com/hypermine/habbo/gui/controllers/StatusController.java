package com.hypermine.habbo.gui.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

/**
 * Created by Scott Stamp <scott@hypermine.com> on 3/20/2017.
 */
public class StatusController {
    @FXML
    public TextArea txtLog;

    public void log(String text) {
        Platform.runLater(() -> txtLog.appendText(text + "\r\n"));
    }
}
