package com.hypermine.habbo;

import com.hypermine.habbo.gui.controllers.StatusController;
import com.hypermine.habbo.utils.Log;
import com.hypermine.habbo.utils.SWFData;
import com.jpexs.decompiler.flash.tags.DefineBinaryDataTag;
import com.jpexs.decompiler.flash.tags.Tag;
import com.jpexs.decompiler.flash.tags.base.ImageTag;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class DownloadFurniSprites implements Runnable {
    private String savePath;
    private int revision;
    private String className;
    private Boolean extractSprites;
    private Boolean extractIcons;
    private StatusController statusController;

    public DownloadFurniSprites(String savePath, int revision, String className, Boolean extractSprites, Boolean extractIcons, StatusController statusController) {
        this.savePath = savePath;
        this.revision = revision;
        this.className = className;
        this.extractSprites = extractSprites;
        this.extractIcons = extractIcons;
        this.statusController = statusController;
    }

    private void makeDirectory(String path)
    {
        File dir = new File(path);
        if (!dir.exists())
        {
            try {
                dir.mkdirs();
            } catch (SecurityException se) {}
        }
    }

    private BufferedImage deepCopy(BufferedImage bi)
    {
        if (bi == null) {
            return null;
        }

        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(bi.getRaster().createCompatibleWritableRaster());
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }


    private static void recolor(BufferedImage image, String c) {
        Color maskColor = Color.decode("#" + c);
        try {
            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < image.getHeight(); y++) {
                    int pixel = image.getRGB(x, y);
                    if (pixel >> 24 != 0) {
                        Color color = new Color(pixel);
                        float red = color.getRed() / 255.0F * (maskColor.getRed() / 255.0F);
                        float green = color.getGreen() / 255.0F * (maskColor.getGreen() / 255.0F);
                        float blue = color.getBlue() / 255.0F * (maskColor.getBlue() / 255.0F);

                        color = new Color(red, green, blue);
                        int rgb = color.getRGB();
                        image.setRGB(x, y, rgb);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static BufferedImage trimImage(BufferedImage image) {
        WritableRaster raster = image.getAlphaRaster();
        int width = raster.getWidth();
        int height = raster.getHeight();
        int left = 0;
        int top = 0;
        int right = width - 1;
        int bottom = height - 1;
        int minRight = width - 1;
        int minBottom = height - 1;

        top:
        for (;top < bottom; top++){
            for (int x = 0; x < width; x++){
                if (raster.getSample(x, top, 0) != 0){
                    minRight = x;
                    minBottom = top;
                    break top;
                }
            }
        }

        left:
        for (;left < minRight; left++){
            for (int y = height - 1; y > top; y--){
                if (raster.getSample(left, y, 0) != 0){
                    minBottom = y;
                    break left;
                }
            }
        }

        bottom:
        for (;bottom > minBottom; bottom--){
            for (int x = width - 1; x >= left; x--){
                if (raster.getSample(x, bottom, 0) != 0){
                    minRight = x;
                    break bottom;
                }
            }
        }

        right:
        for (;right > minRight; right--){
            for (int y = bottom; y >= top; y--){
                if (raster.getSample(right, y, 0) != 0){
                    break right;
                }
            }
        }

        return image.getSubimage(left, top, right - left + 1, bottom - top + 1);
    }

    @Override
    public void run() {
        try {
            //URL swfURL = new URL("https:" + SpriteExtractor.externalVariables.getFlashDynamicDownloadURL() + revision + "/" + className + ".swf");
            URL swfURL = new URL("file:///C:/xampp/htdocs/chocolatey-2.270/gamedata/dcr/hof_furni/" + revision + "/" + className + ".swf");
            String swfPath = savePath + File.separator + "furni" + File.separator;
            String spritePath = savePath + File.separator + "sprites" + File.separator + className + File.separator;
            String iconPath = savePath + File.separator + "icons" + File.separator;

            makeDirectory(swfPath);
            makeDirectory(spritePath);
            makeDirectory(iconPath);

//            statusController.log("Downloading: " + swfURL.toString());

            ReadableByteChannel readableByteChannel = Channels.newChannel(swfURL.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(swfPath + className + ".swf");
            fileOutputStream.getChannel().transferFrom(readableByteChannel,0, Long.MAX_VALUE);

            if (!Files.isDirectory(Paths.get(spritePath))) Files.createDirectory(Paths.get(spritePath));

            SWFData swfData = new SWFData(swfURL);

            for (ImageTag image : swfData.getImageTags()) {
                //FileOutputStream fos = new FileOutputStream(spritePath + image.getClassName() + ".png");
                ImageIO.write(image.getImageCached().getBufferedImage(), "png", new File(spritePath + image.getClassName() + ".png"));
                //fos.close();
                //System.out.println(image.getClassName());
            }

            for (DefineBinaryDataTag xml : swfData.getXmlTags()) {
                //System.out.println(tagData);
                //System.out.println(xml.getClassName());
                FileOutputStream fos = new FileOutputStream(spritePath + xml.getClassName() + ".xml");
                fos.write(xml.binaryData.getRangeData());
                fos.close();
            }

            /*if (extractSprites) {
//                statusController.log("Extracting Sprites: " + swfURL.toString());

                for (ImageTag tag : swfData.getImageTags()) {
                    String spriteName = Arrays.stream(tag.getClassName().split(className + "_"))
                            .distinct().collect(Collectors.joining(className + "_"));
                    if (spriteName.contains("_32_")) continue;
                    if (spriteName.contains("_icon_")) continue;
                    ImageIO.write(tag.getImageCached().getBufferedImage(), "png",
                            new File(spritePath + className + "_icon" + ".png"));
                }
            }

            if (extractIcons) {
                HashMap<Integer, SWFData.MaskColor> colors = swfData.getIconColors();
                HashMap<Integer, SWFData.IconLocation> assetNames = swfData.getAssetNames();
                HashMap<String, BufferedImage> iconImages = new HashMap<>();

//                statusController.log("Extracting Icons: " + swfURL.toString());

                for (ImageTag tag : swfData.getImageTags()) {
                    String spriteName = Arrays.stream(tag.getClassName().split(className + "_"))
                            .distinct().collect(Collectors.joining(className + "_"));

                    if (spriteName.contains("_icon")) {
                        iconImages.put(spriteName, tag.getImageCached().getBufferedImage());
                    }
                }

                if (colors.values().size() == 0)
                {
                    BufferedImage canvas = new BufferedImage(1000, 1000, 2);
                    int basex = 20;
                    int basey = 20;

                    for (int i = 0; i < iconImages.size(); i++) {
                        SWFData.IconLocation location = assetNames.get(i);
                        BufferedImage layer = deepCopy(iconImages.get(location.name));

                        if (basex == 0 || basey == 0) {
                            basex = location.x;
                            basey = location.y;
                        }

                        int offsetx = basex - location.x;
                        int offsety = basey - location.y;

                        if (offsetx < 0) {
                            basex += offsetx * -1;
                            offsetx += offsetx * -1;
                        }
                        if (offsety < 0) {
                            basey += offsety * -1;
                            offsety += offsety * -1;
                        }

                        Graphics g = canvas.getGraphics();
                        g.drawImage(layer, offsetx, offsety, null);
                    }

                    canvas = trimImage(canvas);

                    if (canvas.getWidth() == 1 && canvas.getHeight() == 1)
                    {
                        try {
                            // fucking parser idek what's going on here tbh
                            URL url = new URL("https://images.habbo.com/dcr/hof_furni/" + revision + "/" + className + "_icon.png");
                            readableByteChannel = Channels.newChannel(url.openStream());
                            fileOutputStream = new FileOutputStream(iconPath + className + "_icon.png");
                            fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                        } catch (Exception e) {
                            // failed to download the icon for w/e reason
                            e.printStackTrace();
                        }
                    } else {
                        File icon = new File(iconPath + className  + "_icon" + ".png");
                        ImageIO.write(canvas, "png", icon);
                    }
                }

                for (SWFData.MaskColor maskColor : colors.values()) {
                    BufferedImage canvas = new BufferedImage(1000, 1000, 2);
                    int basex = 20;
                    int basey = 20;

                    for (int i = 0; i < iconImages.size(); i++) {
                        SWFData.IconLocation location = assetNames.get(i);
                        BufferedImage layer = deepCopy(iconImages.get(location.name));

                        if (basex == 0 || basey == 0) {
                            basex = location.x;
                            basey = location.y;
                        }

                        int offsetx = basex - location.x;
                        int offsety = basey - location.y;

                        if (offsetx < 0) {
                            basex += offsetx * -1;
                            offsetx += offsetx * -1;
                        }
                        if (offsety < 0) {
                            basey += offsety * -1;
                            offsety += offsety * -1;
                        }

                        if (maskColor.layerId == i && !maskColor.color.equals("")) // only recolor new layers I guess?
                            recolor(layer, maskColor.color);

                        Graphics g = canvas.getGraphics();
                        g.drawImage(layer, offsetx, offsety, null);
                    }

                    canvas = trimImage(canvas);

                    if (canvas.getWidth() == 1 && canvas.getHeight() == 1)
                    {
                        try {
                            // fucking parser idek what's going on here tbh
                            URL url = new URL("https://images.habbo.com/dcr/hof_furni/" + revision + "/" + className + "_" + maskColor.id + "_icon.png");
                            readableByteChannel = Channels.newChannel(url.openStream());
                            fileOutputStream = new FileOutputStream(iconPath + className + "_" + maskColor.id + "_icon.png");
                            fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                        } catch (Exception e) {
                            // failed to download the icon for w/e reason
                            e.printStackTrace();
                        }
                    } else {
                        File icon = new File(iconPath + className + "_" + maskColor.id + "_icon" + ".png");
                        ImageIO.write(canvas, "png", icon);
                    }
                }
            }*/
        } catch (MalformedURLException ex) {
            Log.error("Malformed URL!", ex);
        } catch (Throwable ex) {
            Log.error("Failed to load/extract SWF!", ex);
        }
    }
}
