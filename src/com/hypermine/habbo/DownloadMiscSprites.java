package com.hypermine.habbo;

import com.hypermine.habbo.gui.controllers.StatusController;
import com.hypermine.habbo.utils.Log;
import com.hypermine.habbo.utils.SWFData;
import com.jpexs.decompiler.flash.tags.base.ImageTag;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by Scott on 8/18/2015.
 */
public class DownloadMiscSprites implements Runnable {
    private final String savePath;
    private final String className;
    private final Boolean stripClass;
    private final Boolean extractSprites;
    private final StatusController statusController;

    public DownloadMiscSprites(String savePath, String className, Boolean stripClass, Boolean extractSprites, StatusController statusController) {
        this.savePath = savePath;
        this.className = className;
        this.stripClass = stripClass;
        this.extractSprites = extractSprites;
        this.statusController = statusController;
    }

    @Override
    public void run() {
        try {
            URL swfURL = new URL("https:" + SpriteExtractor.externalVariables.getFlashClientURL() + className + ".swf");
            String path = savePath + File.separator + "misc" + File.separator;
            String spritePath = savePath + File.separator + "sprites" + File.separator;

            File dir = new File(path);
            if (!dir.exists()) {
                try {
                    dir.mkdirs();
                } catch (SecurityException se) {}
            }

            dir = new File(spritePath);
            if (!dir.exists()) {
                try {
                    dir.mkdirs();
                } catch (SecurityException se) {}
            }

            statusController.log("Downloading: " + swfURL.toString());

            String fileName = swfURL.toString().substring(swfURL.toString().lastIndexOf('/')+1, swfURL.toString().length());

            ReadableByteChannel readableByteChannel = Channels.newChannel(swfURL.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(path + fileName);
            fileOutputStream.getChannel().transferFrom(readableByteChannel,0, Long.MAX_VALUE);

            if (extractSprites) {
                statusController.log("Extracting: " + swfURL.toString());
                SWFData swfData = new SWFData(swfURL);

                for (ImageTag tag : swfData.getImageTags()) {
                    String spriteName;
                    if (stripClass) {
                        spriteName = tag.getClassName().replace(className + "_", "");
                    } else {
                        spriteName = Arrays.stream(tag.getClassName().split(className + "_"))
                                .distinct().collect(Collectors.joining(className + "_"));
                    }
                    if (spriteName.contains("_32_")) continue;
                    ImageIO.write(tag.getImageCached().getBufferedImage(), "png",
                            new File(spritePath + spriteName + ".png"));
                }
            }
        } catch (MalformedURLException ex) {
            Log.error("Malformed URL!", ex);
        } catch (Throwable ex) {
            Log.error("Failed to load/extract SWF!", ex);
        }
    }
}
