package com.hypermine.habbo.xml.swf.assets;

import com.hypermine.habbo.utils.Log;
import com.hypermine.habbo.xml.swf.visualization.VisualizationData;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * Created by Scott Stamp <scott@hypermine.com> on 3/20/2017.
 */
public class AssetsParser {
    public static Assets getAssetsData(String xml) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Assets.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();

            StringReader reader = new StringReader(xml);
            return ((Assets) unmarshaller.unmarshal(reader));
        } catch (JAXBException jaxbEx) {
            Log.error("Failed to create new JAXB instance", jaxbEx);
            return null;
        }
    }
}
