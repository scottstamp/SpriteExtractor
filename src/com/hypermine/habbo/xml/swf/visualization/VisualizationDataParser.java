package com.hypermine.habbo.xml.swf.visualization;

import com.hypermine.habbo.utils.Log;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * Created by Scott Stamp <scott@hypermine.com> on 3/20/2017.
 */
public class VisualizationDataParser {
    public static VisualizationData getVisualizationData(String xml) {
        try {
            JAXBContext jc = JAXBContext.newInstance(VisualizationData.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();

            StringReader reader = new StringReader(xml);
            return ((VisualizationData) unmarshaller.unmarshal(reader));
        } catch (JAXBException jaxbEx) {
            Log.error("Failed to create new JAXB instance", jaxbEx);
            return null;
        }
    }
}
