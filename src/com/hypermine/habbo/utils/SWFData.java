package com.hypermine.habbo.utils;

import com.hypermine.habbo.xml.swf.assets.Assets;
import com.hypermine.habbo.xml.swf.assets.AssetsParser;
import com.hypermine.habbo.xml.swf.visualization.VisualizationData;
import com.hypermine.habbo.xml.swf.visualization.VisualizationDataParser;
import com.jpexs.decompiler.flash.SWF;
import com.jpexs.decompiler.flash.tags.DefineBinaryDataTag;
import com.jpexs.decompiler.flash.tags.Tag;
import com.jpexs.decompiler.flash.tags.base.ImageTag;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Azure Camera Server - Furni Sprite Extractor
 * Written by Scott Stamp (scottstamp851, scott@hypermine.com)
 */

public class SWFData {
    private SWF swf;

    public SWFData(String fileLoc) throws IOException, InterruptedException {
        swf = new SWF(new FileInputStream(fileLoc), false, false);
    }

    public SWFData(URL fileLoc) throws IOException, InterruptedException {
        swf = new SWF(fileLoc.openStream(), false, false);
    }

    public List<Tag> getTags() {
        return swf.getTags().toArrayList();
    }

    public List<ImageTag> getImageTags() {
        return swf.getTags().toArrayList().stream()
                .filter((tag) -> tag.getTagName().equals("DefineBitsLossless2"))
                .map((tag) -> swf.getImage(Integer.parseInt(tag.toString().split("\\(")[1].split(":")[0])))
                .collect(Collectors.toList());
    }

    public List<DefineBinaryDataTag> getXmlTags() {
        return swf.getTags().toArrayList().stream()
                .filter((tag) -> tag.getTagName().equals("DefineBinaryData"))
                .filter((tag) -> ((DefineBinaryDataTag) tag).getClassName().contains("_assets") ||
                        ((DefineBinaryDataTag)tag).getClassName().contains("_logic") ||
                        ((DefineBinaryDataTag)tag).getClassName().contains("_visualization") ||
                        ((DefineBinaryDataTag)tag).getClassName().contains("_index") ||
                        ((DefineBinaryDataTag)tag).getClassName().contains("_manifest"))
                .map((tag) -> (DefineBinaryDataTag)tag)
                .collect(Collectors.toList());
    }

    public HashMap<String, String> getBinaryData() {
        HashMap<String,String> binaryData = new HashMap<String,String>();
        for (Tag tag : swf.getTags().toArrayList())
        {
            if (tag instanceof DefineBinaryDataTag)
            {
                binaryData.put(((DefineBinaryDataTag) tag).getClassName(),
                        new String(((DefineBinaryDataTag) tag).binaryData.getRangeData()));
            }
        }

        return binaryData;
    }

    public VisualizationData getVisualizationData() {
        for (Tag tag : swf.getTags().toArrayList())
        {
            if (tag instanceof DefineBinaryDataTag)
            {
                if (((DefineBinaryDataTag) tag).getClassName().contains("_visualization")) {
                    DefineBinaryDataTag dataTag = (DefineBinaryDataTag) tag;
                    return VisualizationDataParser.getVisualizationData(new String(dataTag.binaryData.getRangeData()));
                }
            }
        }

        return null;
    }

    public Assets getAssetData() {
        try {
            for (Tag tag : swf.getTags().toArrayList()) {
                if (tag instanceof DefineBinaryDataTag) {
                    if (((DefineBinaryDataTag) tag).getClassName().contains("_assets")) {
                        DefineBinaryDataTag dataTag = (DefineBinaryDataTag) tag;
                        return AssetsParser.getAssetsData(new String(dataTag.binaryData.getRangeData()));
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(swf.getShortFileName());
        }

        return null;
    }

    public HashMap<Integer, IconLocation> getAssetNames()
    {
        HashMap<Integer, IconLocation> assetHashMap = new HashMap<>();

        for (Assets.Asset asset : getAssetData().getAsset())
        {
            int x = 0;
            int y = 0;
            if (asset.getX() != null) x = asset.getX();
            if (asset.getY() != null) y = asset.getY();

            IconLocation iconLocation = new IconLocation(asset.getName(), x, y);

            if (asset.getName().contains("_icon_a"))
                assetHashMap.put(0, iconLocation);
            if (asset.getName().contains("_icon_b"))
                assetHashMap.put(1, iconLocation);
            if (asset.getName().contains("_icon_c"))
                assetHashMap.put(2, iconLocation);
            if (asset.getName().contains("_icon_d"))
                assetHashMap.put(3, iconLocation);
        }

        return assetHashMap;
    }

    public HashMap<Integer, MaskColor> getIconColors() {
        HashMap<Integer, MaskColor> colorHashMap = new HashMap<>();

        try {
            for (VisualizationData.Graphics.Visualization viz : getVisualizationData().getGraphics().getVisualization()) {
                if (viz.getSize().equals(1)) {
                    if (viz.getColors() == null) return colorHashMap;
                    for (VisualizationData.Graphics.Visualization.Colors.Color color : viz.getColors().getColor()) {
                        MaskColor mask = new MaskColor(color.getId(),
                                color.getColorLayer().get(0).getId(),
                                color.getColorLayer().get(0).getColor());

                        colorHashMap.put(color.getId(), mask);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return colorHashMap;
    }

    public List<Tag> getXmlData() {
        return swf.getTags().toArrayList().stream()
                .filter((tag) -> tag.getTagName().equals("DefineBinaryData"))
                .collect(Collectors.toList());
    }

    public class MaskColor {
        public final int id;
        public final int layerId;
        public final String color;

        public MaskColor(int id, int layerId, String color)
        {
            this.id = id;
            this.layerId = layerId;
            this.color = color;
        }
    }

    public class IconLocation {
        public final String name;
        public final int x;
        public final int y;

        public IconLocation(String name, int x, int y)
        {
            this.name = name;
            this.x = x;
            this.y = y;
        }
    }
}
