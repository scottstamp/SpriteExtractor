package com.hypermine.habbo.catalog.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Scott Stamp <scott@hypermine.com> on 6/12/2017.
 */
public class CatalogPage {
    private int id;
    private String link;

    public CatalogPage(ResultSet data) throws SQLException {
        this.id = data.getInt("id");
        this.link = data.getString("link");
    }
}
