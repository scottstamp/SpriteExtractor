package com.hypermine.habbo.catalog.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Scott Stamp <scott@hypermine.com> on 6/12/2017.
 */
public class Furniture {
    private final String publicName;
    private final String spriteName;
    private final int isWalkable;
    private final int canSit;
    private final int spriteId;
    private final int revision;
    private final String description;
    private final int specialType;
    private final int canLayOn;

    public Furniture(String publicName, String spriteName, int isWalkable, int canSit, int spriteId, int revision, String description, int specialType, int canLayOn) {
        this.publicName = publicName;
        this.spriteName = spriteName;
        this.isWalkable = isWalkable;
        this.canSit = canSit;
        this.spriteId = spriteId;
        this.revision = revision;
        this.description = description;
        this.specialType = specialType;
        this.canLayOn = canLayOn;
    }

    public Furniture(ResultSet data) throws SQLException {
        this.publicName = data.getString("public_name");
        this.spriteName = data.getString(""); // unfinished...
    }

    public void InsertRow(Connection conn) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(
                "INSERT INTO furniture (public_name, item_name, is_walkable, can_sit, sprite_id, revision, description, specialtype, canlayon) VALUES " +
                        "(?, ?, ?, ?, ?, ?, ?, ?, ?)");

        stmt.setString(1, publicName);
        stmt.setString(2, spriteName);
        stmt.setString(3, isWalkable + "");
        stmt.setString(4, canSit + "");
        stmt.setInt(5, spriteId);
        stmt.setInt(6, revision);
        stmt.setString(7, description);
        stmt.setInt(8, specialType);
        stmt.setString(9, canLayOn + "");

        stmt.execute();
    }
}
