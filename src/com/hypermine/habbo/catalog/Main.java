package com.hypermine.habbo.catalog;

import com.hypermine.habbo.catalog.sql.Furniture;
import com.hypermine.habbo.catalog.xml.FurnidataObj;
import com.hypermine.habbo.xml.furnidata.Furnidata;
import com.hypermine.habbo.xml.furnidata.FurnidataParser;
import com.hypermine.habbo.xml.furnidata.Furnitype;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Scott Stamp <scott@hypermine.com> on 6/12/2017.
 */
public class Main {
    private static Furnidata furniData;
    private static List<Furnitype> roomFurni;
    private static List<Furnitype> wallFurni;

    private static Map<String, List<Integer>> lines = new HashMap<>();

    private static Map<String, Integer> catalogPages = new HashMap<>();

    private static Connection sqlConnection;

    private static final String selectCatalogPages =
            "SELECT id, link FROM catalog_pages WHERE parent_id = 1";

    private static final String insertCatalogPage =
            "INSERT INTO catalog_pages (parent_id,caption,order_num,link,page_images,page_texts) VALUES(?, ?, ?, ?, '', '')";

    public static void main(String args[]) {
        try {
            sqlConnection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/catalog_generator?user=root&password=");
            //getCatalogPages();
            //getFurniData();

            for (Furnitype type : roomFurni) {
                try {
                    insertFurniture(type);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            for (Furnitype type : wallFurni) {
                try {
                    insertFurniture(type);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            /*for (Furnitype type : roomFurni) {
                if (!lines.containsKey(type.getFurniline()))
                    lines.put(type.getFurniline(), new ArrayList<>());
                lines.get(type.getFurniline()).add(type.getId());
            }

            for (Furnitype type : wallFurni) {
                if (!lines.containsKey(type.getFurniline()))
                    lines.put(type.getFurniline(), new ArrayList<>());
                lines.get(type.getFurniline()).add(type.getId());
            }

            for (Map.Entry<String, List<Integer>> line : lines.entrySet()) {
                PreparedStatement stmt = sqlConnection.prepareStatement(insertCatalogPage);
                stmt.setInt(1, 1);
                stmt.setString(2, line.getKey());
                stmt.setInt(3, 0);
                stmt.setString(4, line.getKey());
                stmt.execute();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void getCatalogPages() throws SQLException {
        ResultSet pageRs = sqlConnection.prepareStatement(selectCatalogPages).executeQuery();

        while (pageRs.next()) {
            catalogPages.put(pageRs.getString("link"), pageRs.getInt("id"));
        }
    }

    private static void getFurniData() {
        furniData = FurnidataParser.getFurnidata();
        assert furniData != null;
        roomFurni = furniData.getRoomitemtypes().getFurnitype();
        wallFurni = furniData.getWallitemtypes().getFurnitype();
    }

    private static void insertFurniture(Furnitype type) throws SQLException {
        int pageId = catalogPages.get(type.getFurniline());
        String pageLink = type.getFurniline();

        String publicName = type.getName();
        String className = type.getClassname();
        int canstandon = type.getCanstandon();
        int cansiton = type.getCansiton();
        int id = type.getId();
        int revision = type.getRevision();
        String description = type.getDescription();
        int specialType = type.getSpecialtype();
        int canlayon = type.getCanlayon();

        Furniture furniture = new Furniture(publicName, className, canstandon, cansiton, id, revision, description, specialType, canlayon);
        furniture.InsertRow(sqlConnection);

        System.out.format("Found item ID [%d - %s], page [%d - %s]\r\n", type.getId(), type.getClassname(), pageId, pageLink);
    }
}
