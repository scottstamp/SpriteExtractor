package com.hypermine.habbo.catalog.xml;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Created by Scott Stamp <scott@hypermine.com> on 6/12/2017.
 */
public class FurnidataObj {
    private Document doc;

    public FurnidataObj() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        doc = db.parse("https://www.habbo.com/gamedata/furnidata_xml/9796824b30a5028bfca84200851b0896838d67d0");
    }

    public NodeList getFloorItems() {
        NodeList nodes = doc.getDocumentElement().getChildNodes();
        return nodes.item(0).getChildNodes();
    }
}
