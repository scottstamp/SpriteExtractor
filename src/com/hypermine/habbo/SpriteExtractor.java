package com.hypermine.habbo;

import com.hypermine.habbo.gui.controllers.StatusController;
import com.hypermine.habbo.xml.effectmap.Effectmap;
import com.hypermine.habbo.txt.ExternalVariables;
import com.hypermine.habbo.xml.effectmap.EffectmapParser;
import com.hypermine.habbo.xml.figuremap.Figuremap;
import com.hypermine.habbo.xml.figuremap.FiguremapParser;
import com.hypermine.habbo.xml.furnidata.Furnidata;
import com.hypermine.habbo.xml.furnidata.FurnidataParser;
import com.hypermine.habbo.xml.furnidata.Furnitype;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Azure Camera Server - Furni Sprite Extractor
 * Written by Scott Stamp (scottstamp851, scott@hypermine.com)
 */
public class SpriteExtractor implements Runnable {
    public static ExternalVariables externalVariables;
    private final StatusController statusController;
    private String savePath;
    private boolean downloadMisc, downloadPets, downloadFigures, downloadEffects, downloadMasks, downloadFurni,
                    extractSprites, extractIcons;

    public SpriteExtractor(String savePath, String hotel, StatusController statusController,
                           boolean downloadMisc, boolean downloadPets, boolean downloadFigures,
                           boolean downloadEffects, boolean downloadMasks, boolean downloadFurni,
                           boolean extractSprites, boolean extractIcons) {

        try {
            externalVariables = new ExternalVariables(new URL(hotel));
        } catch (Throwable throwable) {
            statusController.log("Failed to load External Variables from URL: " + hotel);
            statusController.log(throwable.getMessage());
        }

        this.savePath = savePath;
        this.statusController = statusController;
        this.downloadMisc = downloadMisc;
        this.downloadPets = downloadPets;
        this.downloadFigures = downloadFigures;
        this.downloadEffects = downloadEffects;
        this.downloadMasks = downloadMasks;
        this.downloadFurni = downloadFurni;
        this.extractSprites = extractSprites;
        this.extractIcons = extractIcons;
    }

    public void run() {
        ExecutorService downloadPool = Executors.newFixedThreadPool(4);
        List<Callable<Object>> downloadTasks = new ArrayList<>();

        try {
            // Download misc. files (specified in config_habbo.xml)
            if (downloadMisc) downloadMisc(downloadTasks);
            // Download all pet sprites
            if (downloadPets) downloadPets(downloadTasks);
            // Download all figure sprites
            if (downloadFigures) downloadFigures(downloadTasks);
            // Download all effects sprites
            if (downloadEffects) downloadEffects(downloadTasks);
            // Download all wall/floor/landscape masks
            if (downloadMasks) downloadMasks(downloadTasks);
            // Download all furni sprites
            if (downloadFurni) downloadFurni(downloadTasks);

            downloadPool.invokeAll(downloadTasks);
        } catch (Throwable e) {
            statusController.log("Exception: " + e.getMessage());
        }
    }

    private static void listFurni() {
        Furnidata furnidata = FurnidataParser.getFurnidata();
        if (furnidata != null) {
            List<Furnitype> roomItemTypes
                    = furnidata.getRoomitemtypes().getFurnitype();

            for (Furnitype item : roomItemTypes) {
                System.out.println(item.getClassname() + " - " + item.getName());
            }
        }
    }

    private static void formatInteractionType(String line, Furnidata furnidata) {
        if (furnidata != null) {
            List<Furnitype> roomItemTypes
                    = furnidata.getRoomitemtypes().getFurnitype();

            List<Furnitype> wallItemTypes
                    = furnidata.getWallitemtypes().getFurnitype();

            String furniName = line.split(":")[0];
            String interactionType = line.split(":")[1];
            ArrayList<Furnitype> wallItems =
                    wallItemTypes.stream()
                            .filter((item) -> item.getClassname().equals(furniName))
                            .collect(Collectors.toCollection(ArrayList::new));

            for (Furnitype wallItem : wallItems)
                if (!wallItem.equals(null))
                    System.out.println(furniName + ":" + wallItem.getId() + ":" + interactionType);

            wallItems = wallItemTypes.stream()
                    .filter(item -> item.getClassname().startsWith(furniName) && item.getClassname().contains("*"))
                    .collect(Collectors.toCollection((ArrayList::new)));

            for (Furnitype wallItem : wallItems)
                if (!wallItem.equals(null))
                    System.out.println(wallItem.getClassname() + ":" + wallItem.getId() + ":" + interactionType);
        }
    }

    private void downloadFurni(List<Callable<Object>> downloadTasks) {
        Furnidata furnidata = FurnidataParser.getFurnidata();
        if (furnidata != null) {
            List<Furnitype> roomItemTypes
                    = furnidata.getRoomitemtypes().getFurnitype();

            List<Furnitype> wallItemTypes
                    = furnidata.getWallitemtypes().getFurnitype();

            List<String> itemClassNames = new ArrayList<>();

            for (Furnitype item : roomItemTypes) {
                //int id = item.getId();
                //String classname = item.getClassname();
                //String publicname = item.getName();

                //System.out.println("UPDATE items_base SET public_name = \"" + publicname + "\" AND item_name = \"" + classname + "\" WHERE sprite_id = " + id + ";");
                //System.out.print("[\"" + classname + "\",\"" + publicname + "\"],");

                String className = item.getClassname().split("\\*")[0];
                if (!itemClassNames.contains(className))
                    downloadTasks.add(Executors.callable(
                            new DownloadFurniSprites(savePath, item.getRevision(), className,
                                    extractSprites, extractIcons, statusController)));

                itemClassNames.add(className);
            }

            for (Furnitype item : wallItemTypes) {
                int id = item.getId();
                String classname = item.getClassname();
                String publicname = item.getName();

                //System.out.println("UPDATE items_base SET public_name = \"" + publicname + "\" AND item_name = \"" + classname + "\" WHERE sprite_id = " + id + ";");
                //System.out.print("[\"" + classname + "\",\"" + publicname + "\"],");

                String className = item.getClassname().split("\\*")[0];
                if (!itemClassNames.contains(className))
                    downloadTasks.add(Executors.callable(
                            new DownloadFurniSprites(savePath, item.getRevision(), className,
                                    extractSprites, extractIcons, statusController)));

                itemClassNames.add(className);
            }
        }
    }

    private void downloadMasks(List<Callable<Object>> downloadTasks) {
        downloadTasks.add(Executors.callable(
                new DownloadMiscSprites(savePath, "HabboRoomContent", true,
                        extractSprites, statusController)));
    }

    private void downloadEffects(List<Callable<Object>> downloadTasks) throws MalformedURLException {
        Effectmap effectmap = EffectmapParser.getEffectmap(
                new URL("https:" + externalVariables.getFlashClientURL() + "effectmap.xml"));

        if (effectmap != null) {
            downloadTasks.addAll(effectmap.getEffects().stream()
                    .map(effect -> Executors.callable(
                            new DownloadMiscSprites(savePath, effect.getLib(), true,
                                    extractSprites, statusController)))
                    .collect(Collectors.toList()));
        }
    }

    private void downloadFigures(List<Callable<Object>> downloadTasks) throws MalformedURLException {
        Figuremap figuremap = FiguremapParser.getFiguremap(
                new URL("https:" + externalVariables.getFlashClientURL() + "figuremap.xml"));

        if (figuremap != null) {
            downloadTasks.addAll(figuremap.getLib().stream()
                    .map(lib -> Executors.callable(
                            new DownloadMiscSprites(savePath, lib.getId(), true,
                                    extractSprites, statusController)))
                    .collect(Collectors.toList()));
        }
    }

    private void downloadPets(List<Callable<Object>> downloadTasks) {
        downloadTasks.addAll(externalVariables.getPets().stream()
                .map(pet -> Executors.callable(
                        new DownloadMiscSprites(savePath, pet, false,
                                extractSprites, statusController)))
                .collect(Collectors.toList()));
    }

    private void downloadMisc(List<Callable<Object>> downloadTasks) {
        downloadTasks.add(Executors.callable(
                new DownloadMiscSprites(savePath,"hh_human_body", true,
                        extractSprites, statusController)));
        downloadTasks.add(Executors.callable(
                new DownloadMiscSprites(savePath,"hh_human_fx", true,
                        extractSprites, statusController)));
        downloadTasks.add(Executors.callable(
                new DownloadMiscSprites(savePath,"hh_human_item", true,
                        extractSprites, statusController)));
    }
}
